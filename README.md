# NE:ONE OkHttp Client

OkHttp based HTTP Client for One Record server. Client generated based on available OpenAPI specification.

## Usage

Include the following dependency in your project:

```xml

<dependency>
    <groupId>aero.gobeyond.odett</groupId>
    <artifactId>neone-client-okhttp</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```

Then, create a new instance of the client:

```java
// Create access token provider
final AccessTokenProvider accessTokenProvider = new DefaultAccessTokenProvider(
    new OkHttpClient.Builder().build(),
    "<CLIENT_ID>",
    "<CLIENT_SECRET>",
    "https://auth.one-record.lhind.dev/b8a746a6-c540-43f4-92f3-6150ad057215/b2c_1a_hackathon/oauth2/v2.0/token",
    "https://auth.one-record.lhind.dev/3bb1f105-4430-4c46-b35f-7b093ffe5fc6/.default"
);

// http client with authenticator
final OkHttpClient underTest = new OkHttpClient.Builder()
    .authenticator(new AccessTokenAuthenticator(accessTokenProvider))
    .build();

// neone logistic object api
final LogisticsObjectControllerApi api = new NeoneClientOkHttp(underTest, "https://go-beyond.one-record.lhind.dev")
        .logisticsObjectApi();

// make api calls
...
```
