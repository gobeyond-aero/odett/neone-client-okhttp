package aero.gobeyond.odett.neone.client.okhttp.interceptor;

public interface AccessTokenProvider {
    /**
     * Returns a valid access token.
     */
    String getAccessToken();
}
