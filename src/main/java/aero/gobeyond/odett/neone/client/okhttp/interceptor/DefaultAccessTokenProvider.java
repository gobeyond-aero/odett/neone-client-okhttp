package aero.gobeyond.odett.neone.client.okhttp.interceptor;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class DefaultAccessTokenProvider implements AccessTokenProvider {
    private static final Gson GSON = new Gson();
    private static final int DEFAULT_EXPIRE_THRESHOLD_MS = 2 * 60 * 1000; // 2 minutes
    private final OkHttpClient client;
    private final String clientId;
    private final String clientSecret;
    private final String accessTokenUrl;
    private final String scope;
    private final int expireThreshold;

    private volatile String accessToken;
    private volatile long expiresOn;

    public DefaultAccessTokenProvider(
            String clientId,
            String clientSecret,
            String accessTokenUrl,
            String scope
    ) {
        this(new OkHttpClient.Builder().build(), clientId, clientSecret, accessTokenUrl, scope);
    }

    public DefaultAccessTokenProvider(
            OkHttpClient client,
            String clientId,
            String clientSecret,
            String accessTokenUrl,
            String scope
    ) {
        this(client, clientId, clientSecret, accessTokenUrl, scope, DEFAULT_EXPIRE_THRESHOLD_MS);
    }

    public DefaultAccessTokenProvider(
            OkHttpClient client,
            String clientId,
            String clientSecret,
            String accessTokenUrl,
            String scope,
            int expireThreshold
    ) {
        this.client = client;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.accessTokenUrl = accessTokenUrl;
        this.scope = scope;
        this.expireThreshold = expireThreshold;
    }

    @Override
    @NotNull
    public String getAccessToken() {
        if (isValid()) {
            return accessToken;
        }

        final Request request = buildRequest();
        final long now = System.currentTimeMillis();
        try (final Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Authentication response code was not successful: " + response.code());
            }
            final String body = response.body().string();
            final JsonObject json = GSON.fromJson(body, JsonObject.class);

            if (!json.has("access_token") || !json.has("expires_in")) {
                throw new IOException("Authentication response did not contain access_token and expires_in");
            }

            accessToken = json.get("access_token").getAsString();
            expiresOn = json.get("expires_in").getAsLong() * 1000L + now;
        } catch (IOException e) {
            throw new RuntimeException("Could not authenticate", e);
        }

        return accessToken;
    }

    @NotNull
    private Request buildRequest() {
        return new Request.Builder()
                .url(accessTokenUrl)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .post(new FormBody.Builder()
                        .add("grant_type", "client_credentials")
                        .add("client_id", clientId)
                        .add("client_secret", clientSecret)
                        .add("scope", scope)
                        .build())
                .build();
    }

    private boolean isValid() {
        return accessToken != null && (System.currentTimeMillis() < (expiresOn - expireThreshold));
    }
}
