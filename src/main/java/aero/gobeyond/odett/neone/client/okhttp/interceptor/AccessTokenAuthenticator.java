package aero.gobeyond.odett.neone.client.okhttp.interceptor;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import org.jetbrains.annotations.Nullable;

public class AccessTokenAuthenticator implements Authenticator {
    private final AccessTokenProvider accessTokenProvider;

    public AccessTokenAuthenticator(AccessTokenProvider AccessTokenProvider) {
        this.accessTokenProvider = AccessTokenProvider;
    }

    @Nullable
    @Override
    public Request authenticate(Route route, Response response) {
        if (isRequestWithAccessToken(response)) {
            return null; // Give up, we've already attempted to authenticate.
        }

        synchronized (this) {
            final String newAccessToken = accessTokenProvider.getAccessToken();
            return newRequestWithAccessToken(response.request(), newAccessToken);
        }
    }

    private boolean isRequestWithAccessToken(Response response) {
        String header = response.request().header("Authorization");
        return header != null && header.startsWith("Bearer");
    }


    private Request newRequestWithAccessToken(Request request, String accessToken) {
        return request.newBuilder()
                .header("Authorization", "Bearer " + accessToken)
                .build();
    }
}
