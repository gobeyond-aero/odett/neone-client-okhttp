package aero.gobeyond.odett.neone.client.okhttp;


import aero.gobeyond.odett.neone.client.okhttp.api.*;
import okhttp3.OkHttpClient;

public class NeoneClientOkHttp {
    private final ApiClient apiClient;

    public NeoneClientOkHttp(OkHttpClient client, String basePath) {
        apiClient = new ApiClient(client);
        apiClient.setBasePath(basePath);
    }

    public LogisticsObjectControllerApi logisticsObjectApi() {
        return new LogisticsObjectControllerApi(apiClient);
    }

    public ActionRequestControllerApi actionRequestApi() {
        return new ActionRequestControllerApi(apiClient);
    }

    public AuditTrailControllerApi auditTrailApi() {
        return new AuditTrailControllerApi(apiClient);
    }

    public ExtraLogisticsObjectsControllerApi extraLogisticsObjectApi() {
        return new ExtraLogisticsObjectsControllerApi(apiClient);
    }

    public LogisticsEventControllerApi logisticsEventApi() {
        return new LogisticsEventControllerApi(apiClient);
    }

    public NotificationControllerApi notificationApi() {
        return new NotificationControllerApi(apiClient);
    }

    public ServerInformationControllerApi serverInformationApi() {
        return new ServerInformationControllerApi(apiClient);
    }

    public SubscriptionControllerApi subscriptionApi() {
        return new SubscriptionControllerApi(apiClient);
    }

    public SubscriptionManagementControllerApi subscriptionManagementApi() {
        return new SubscriptionManagementControllerApi(apiClient);
    }
}
