package aero.gobeyond.odett.neone.client.okhttp.interceptor;

import aero.gobeyond.odett.neone.client.okhttp.ApiException;
import aero.gobeyond.odett.neone.client.okhttp.NeoneClientOkHttp;
import aero.gobeyond.odett.neone.client.okhttp.api.LogisticsObjectControllerApi;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO mock the server and use test values
class AccessTokenAuthenticatorTest {

    private final AccessTokenProvider accessTokenProvider = new DefaultAccessTokenProvider(
            "3bb1f105-4430-4c46-b35f-7b093ffe5fc6",
            "bGe8Q~.T02hyZrAe7IaXauv2k3GA-8c6hFem5awC",
            "https://auth.one-record.lhind.dev/b8a746a6-c540-43f4-92f3-6150ad057215/b2c_1a_hackathon/oauth2/v2.0/token",
            "https://auth.one-record.lhind.dev/3bb1f105-4430-4c46-b35f-7b093ffe5fc6/.default"
    );

    @Test
    void testClient() throws IOException, ApiException {
        // given
        final OkHttpClient underTest = new OkHttpClient.Builder()
                .authenticator(new AccessTokenAuthenticator(accessTokenProvider))
                .build();
        final LogisticsObjectControllerApi api = new NeoneClientOkHttp(underTest, "https://go-beyond.one-record.lhind.dev")
                .logisticsObjectApi();

        // when
        final ResponseBody body = api
                .logisticsObjectsIdGetCall("03efe947-841e-4685-abc4-6967c5ec9713", null, null, null)
                .execute()
                .body();

        final ResponseBody body2 = api
                .logisticsObjectsIdGetCall("03efe947-841e-4685-abc4-6967c5ec9713", null, null, null)
                .execute()
                .body();

        // then
        assertNotNull(body);
        assertNotNull(body2);
        assertTrue(body.string().contains("Tony"));
        assertTrue(body2.string().contains("Tony"));
    }
}
